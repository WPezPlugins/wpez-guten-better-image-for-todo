<?php
/**
 * Plugin Name: WPezPlugins: Guten Better - Image Block Customizations for TODO
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-guten-better-image-for-todo
 * Description: Boilerplate "fill in the blanks" customizations for the WPezGutenBetter Image Block plugin.
 * Version: 0.0.9
 * Author: MF Simchock (Chief Executive Alchemist) for Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez-gbi
 *
 * @package WPezGutenBetterImageForTODO
 */

namespace WPezGutenBetterImageForTODO;

/**
 * Check to see if the main plugin is activated.
 *
 * @return boolean
 */
function mainPluginActivated() : bool {

	if ( function_exists( 'WPezGutenBetterImage\initPlugin' ) ) {
		return true;
	}
	return false;
}

/**
 * Adds the filter that "receives" the instance of the main plugin's config object.
 *
 * @return void
 */
function initPluginConfig() {

	if ( false === mainPluginActivated() ) {
		return;
	}

	// Note: If 'WPezGutenBetterImage' or '/expose_config' changes in the main plugin, it must also be updated here. 
	add_filter( 'WPezGutenBetterImage' . '/expose_config', __NAMESPACE__ . '\setPluginConfig' );

}
add_action( 'init', __NAMESPACE__ . '\initPluginConfig', 5 );

/**
 * Callback of the add_filter in initPluginConfig that receives and stores the config.
 *
 * @param boolean $new_config Instance of main plugin's config's class.
 * 
 * @return object
 */
function setPluginConfig( $new_config = false ) : object {

	static $config = '';
	if ( $new_config instanceof \WPezGutenBetterImage\App\Config\ClassConfig ) {
		// Stash a pointer to the config instance.
		$config = $new_config;
		return $new_config;
	}

	return $config;
}

/**
 * Returns the config's value for the method requested.
 *
 * @param string $str_method Config method name.
 *
 * @return array|float|int|string
 */
function getPluginConfig( $str_method = '' ) {

	$new_config = setPluginConfig();
	if ( \method_exists( $new_config, trim( $str_method ) ) ) {
		return $new_config->$str_method();
	}
	return null;

}

/**
 * Inits the plugin's numerous add_filter()s. 
 *
 * @return void
 */
function initAddFilters() {

	if ( false === mainPluginActivated() ) {
		return;
	}

	// The defaults for each filter. Adjust these or the individual filters args as needed.
	// Note: Priority is PHP_INT_MAX so these settings will always take priority (read: go last) over
	// the other UI-based Guten Better Image Block settings plugin. We don't want a user to override
	// the "rules" set by the developer.
	$arr_defaults = array(
		'active'        => false,
		'priority'      => PHP_INT_MAX,
		'accepted_args' => 1,
	); 

	$arr_filters = array();

		// add_filter: contentWidth.
	$arr_filters['alignProcess'] = array(
		'accepted_args' => 3,
	);

	// add_filter: contentWidth.
	$arr_filters['contentWidth'] = array(
		'active'        => true,
		'accepted_args' => 2,
	);

	// add_filter: cssUnit.
	$arr_filters['cssUnit'] = array(
		'active'        => true,
		'accepted_args' => 4,
	);

	// add_filter: sizesAlignFull.
	$arr_filters['sizesAlignFull'] = array(
		'accepted_args' => 3,
	);

	// add_filter: sizesAlignWideRatio.
	$arr_filters['sizesAlignWideRatio'] = array();

	// add_filter: sizesAlignWideFixed.
	$arr_filters['sizesAlignWideFixed'] = array(
		'active' => true,
	);

	// add_filter: sizesAlignWide.
	$arr_filters['sizesAlignFull'] = array(
		'accepted_args' => 3,
	);

	// add_filter: sizesAlignCenter.
	$arr_filters['sizesAlignCenter'] = array(
		'active'        => true,
		'accepted_args' => 4,
	);

	// add_filter: sizesAlignLeft.
	$arr_filters['sizesAlignLeft'] = array(
		'accepted_args' => 4,
	);

	// add_filter: sizesAlignRight.
	$arr_filters['sizesAlignRight'] = array(
		'accepted_args' => 4,
	);

	// add_filter: modifyBlockContent.
	$arr_filters['modifyBlockContent'] = array(
		'accepted_args' => 5,
	);

	// add_filter: srcsetMaxWidthRatio.
	$arr_filters['srcsetMaxWidthRatio'] = array(
		'accepted_args' => 2,
	);

	// add_filter: srcsetMinWidth.
	$arr_filters['srcsetMinWidth'] = array(
		'accepted_args' => 3,
	);

	// add_filter: srcsetMaxWidth.
	$arr_filters['srcsetMaxWidth'] = array(
		'accepted_args' => 3,
	);

	// add_filter: srcsetSources.
	$arr_filters['srcsetSources'] = array(
		'active'        => true,
		'accepted_args' => 5,
	);

	$str_prefix = trim( getPluginConfig( 'getPluginSlug' ) ) . '/';
	foreach ( $arr_filters as $ndx => $arr_filter ) {

		$arr_merge = array_merge( $arr_defaults, $arr_filter );
		// Do we have a filter we want to add? If not, continue.
		if ( ! is_string( $ndx ) || true !== $arr_merge['active'] || ! is_int( $arr_merge['accepted_args'] ) ) {
			continue;
		}

		add_filter( $str_prefix . trim( $ndx ), __NAMESPACE__ . '\\' . trim( $ndx ), $arr_merge['priority'], $arr_merge['accepted_args'] );
	}
}
add_action( 'init', __NAMESPACE__ . '\initAddFilters', 20 );


/**
 * Customize which image block align values we process.
 *
 * @param array  $arr_align_process Current array of align values to be processed.
 * @param string $str_block_content The block content.
 * @param array  $arr_attrs The block's attrs.
 * @return array
 */
function alignProcess( array $arr_align_process = array(), string $str_block_content = '', array $arr_attrs = array() ) : array {

	return $arr_align_process;
}


/**
 * Customize the $content_width.
 *
 * @param float $num Current content width.
 * @param string $context The __FUNCTION__ calling this method.
 *
 * @return float Content width to use.
 */
function contentWidth( float $num = 0, string $context = '' ) : float {

	// This should reflect the actual width of the image content, that is the main content container width minus padding, margins, etc.
	//
	// The goal is that when the img's width in the block editor is set to center we want a sizes that nails that perfectly, else the
	// browser will pick a suboptimal img from the srcset. Finally, your WP (custom) image sizes should align with this so your image aren't
	// any larger than necessary. Got it?

	// Test example
	//  $num = 987.65;
	return $num;
}

/**
 * Customize the css_unit (e.g., px, em, rem) used.
 *
 * @param string $str_unit Current css unit.
 * @param boolean $str_align GB image alignment (e.g., left, center, wide, etc.).
 * @param boolean $str_block_content GB block content (markup).
 * @param boolean $arr_attrs The attrs for the block.
 *
 * @return string Return The css_unit to use.
 */
function cssUnit( string $str_unit = 'px', string $str_align = '', string $str_block_content = '' , array $arr_attrs = array() ) : string {

	// Example
	// $str_unit = 'em';
	return $str_unit;
}


/**
 * Customize the sizes array for align: full.
 *
 * @param array $arr_sizes Array of current sizes.
 * @param string $str_block_content GB block content (markup).
 * @param array $arr_args The attrs for the block.
 * 
 * @return array Array of sizes to use.
 */
function sizesAlignFull( array $arr_sizes = array(), string $str_block_content = '', array $arr_args = array() ) : array {

	return $arr_sizes;
}

/**
 * If you're not certain of the width-maximum for align = wide, you can estimate it. The content_width will
 * be multiplied by the number below.
 *
 * @param float $num_align_wide_ratio
 * 
 * @return float
 */
function sizesAlignWideRatio( float $num_align_wide_ratio = 1.00 ) : float {

	return $num_align_wide_ratio;
}

/**
 * The align wide image width is this fixed value( that will override the one calculated by the align wide ration * content width ).
 *
 * @param float $num_align_wide_fixed $num_align_wide_fixed TODO.
 *
 * @return float
 */
function sizesAlignWideFixed( float $num_align_wide_fixed ) : float {

	// --- TODO ---
	$float_fixed = 1340.0;

	// Uncomment the line below to keep using the defaults created by the main plugin.
	// $num_align_wide_fixed = $float_fixed;
	return $num_align_wide_fixed;
}


/**
 * Customize the sizes array for align: wide.
 *
 * @param array $arr_sizes
 * @param string $str_block_content
 * @param array $arr_args
 *
 * @return array
 */
function sizesAlignWide( array $arr_sizes = array(), string $str_block_content = '', array $arr_args = array() ) : array {

	// --- TODO ---
	$str = '1205px';

	$arr = array();
	$arr[] = '(max-width:' . $str . ') 100vw';
	$arr[] = $str;

	// Uncomment the line below to keep using the defaults created by the main plugin
	// $arr = $arr_sizes;

	return $arr;
}

/**
 * Customize the sizes array for align: center.
 *
 * @param array $arr_sizes
 * @param string $str_block_content
 * @param array $arr_args
 * @param float $num_width
 *
 * @return array
 */
function sizesAlignCenter( array $arr_sizes = array(), string $str_block_content = '', array $arr_args = array(), float $num_width ) : array {

	// --- TODO ---
	$str = '908px';

	$arr = array();
	$arr[] = '(max-width:' . $str . ') 100vw';
	$arr[] = $str;

	// Uncomment the line below to keep using the defaults created by the main plugin
	// $arr = $arr_sizes;

	return $arr;
}


/**
 * Customize the sizes array for align: left.
 *
 * @param array $arr_sizes
 * @param string $str_block_content
 * @param array $arr_args
 * @param float $num_width
 *
 * @return array
 */
function sizesAlignLeft( array $arr_sizes = array(), string $str_block_content = '', array $arr_args = array(), float $num_width ) : array {

	// --- TODO ---
	$str = '720px';

	$arr = array();
	$arr[] = '(max-width:' . $str . ') 100vw';
	$arr[] = $str;

	// Uncomment the line below to keep using the defaults created by the main plugin
	$arr = $arr_sizes;

	return $arr;
}

/**
 * Customize the sizes array for align: left.
 *
 * @param array $arr_sizes
 * @param string $str_block_content
 * @param array $arr_args
 * @param float $num_width
 *
 * @return array
 */
function sizesAlignRight( array $arr_sizes = array(), string $str_block_content = '', array $arr_args = array(), float $num_width ) : array {

	// --- TODO ---
	$str = '620px';

	$arr = array();
	$arr[] = '(max-width:' . $str . ') 100vw';
	$arr[] = $str;

	// Uncomment the line below to keep using the defaults created by the main plugin
	$arr = $arr_sizes;

	return $arr;
}


/**
 * Customize the block content markup.
 *
 * @param string $str_block_content
 * @param string $str_align
 * @param array $arr_sizes
 * @param array $arr_sizes_custom
 * @param array $arr_attrs
 * @param float $num_width
 * 
 * @return string
 */
function modifyBlockContent( string $str_block_content, string $str_align, array $arr_sizes, array $arr_sizes_custom, array $arr_attrs, float $num_width ) : string {

	// --- TODO ---
	return $str_block_content;
}


/**
 * For align: wide, the max width can be calculated (content width * srcset_max_width_ratio).
 *
 * @param float $num_max_width_ratio
 * @param string $str_align
 * 
 * @return float
 */
function srcsetMaxWidthRatio( float $num_max_width_ratio, string $str_align ) : float {

	return $num_max_width_ratio;
}


/**
 * Undocumented function
 *
 * @param integer $int_min_width
 * @param string $str_align
 * @param array $arr_wp_args
 * 
 * @return integer
 */
function srcsetMinWidth( int $int_min_width, string $str_align, array $arr_wp_args ) : int {

	return int_min_width;

}

/**
 * Customize the max width (in px) of images included in the srcset.
 *
 * @param string $int_max_width
 * @param bool $str_align
 * @param bool $arr_args
 * 
 * @return int
 */
function srcsetMaxWidth( int $int_min_width, string $str_align, array $arr_wp_args ) : int {

	global $content_width;
	$num = 1.75;
	// --- TODO ---

	switch ( $str_align ) {

		case 'full':
			$int = 1000;
			break;
		case 'wide':
			$int = 500;
			break;
		case 'center':
			$int = (int) ( $content_width * $num );
			break;
		case 'left':
			$int = (int) ( $content_width * $num );
			break;
		case 'right':
			$int = (int) ( $content_width * $num );
			break;
		default:
			$int = $int_max_width;
	}

	// Uncomment the line below to keep using the defaults created by the main plugin
	// $int = $int_max_width;
	return $int;
}

/**
 * Last chance to customize the $sources array passed in by the filter: wp_calculate_image_srcset
 *
 * @param array   $sources
 * @param array   $size_array
 * @param string  $image_src
 * @param array   $image_meta
 * @param integer $attachment_id
 *
 * @return array
 */
function srcsetSources( array $sources, array $size_array, string $image_src, array $image_meta, int $attachment_id ) : array {

	return $sources;
}
