## WPezGutenBetter Image for TODO

__A boilerplate plugin for your custom settings when using the WPezGutenBetter Image Block plugin (https://gitlab.com/WPezPlugins/wpez-guten-better-image).__



### OVERVIEW

Left to its own devices, WordPress can make suboptimal use of the img tag's sizes="..." and srcset="...' values. The good news is, the Gutenberg Image Block's attributes can be used to fix-up where WP falls short.

If you're not familiar with the img tag's sizes="..." and srcset="...' there are helpful links listed below. We recommend starting with the Eric Portis article.

The README of the WPezPlugins: Guten Better - Image Block plugin (link also listed below) has examples of why friends don't let friends use the Block Editor's Image block without it. 

> ---
>
> IMPORTANT
> 
> This is boilerplate code that you must modify with your own particular settings. 
>
> It is also recommended you update the folder name, file name, and the namespace.
>
> We like to use the client / project's initials, as well as the theme at which these settings are targeted. 
>
> ---

### Getting Started

While the main plugin provides a number of filters for customization, all of which are defined here, in most cases you'll only want to use the contentWidth() filter, and perhaps cssUnit() (if your theme doesn't use 'px' for its width properties). 

This will get you up and running, and in a better, more optimized place than plain ol' vanilla Gutenberg. 

Yes, it's that ez.

Beyond that, there are additional filters that are available when your needs are more particular / exact.


### FYI

In order to enable maximum flexibility, the sizes string (in the main plugin) is built from an array using the PHP function implode. 

https://www.php.net/manual/en/function.implode.php

For example, to yield a sizes of:

(max-width: 1000px) 100vw, 1000px;

You would do this:

    $arr = array();
    $arr[] = '(max-width: 1000px) 100vw';
    $arr[] = '1000px';
    return $arr;
    
The implode function will add the comma(s) between the pieces.

You'll see this pattern repeated throughout this plugin when doing your own settings. 


### Helpful Links

- https://gitlab.com/WPezPlugins/wpez-guten-better-image

- https://ericportis.com/posts/2014/srcset-sizes/

- https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images

- https://make.wordpress.org/core/tag/4-4/#post-15628
 
- https://viastudio.com/optimizing-your-theme-for-wordpress-4-4s-responsive-images/
 
-  https://medium.com/@duongphan/controlling-responsive-images-in-wordpress-breaking-down-the-code-7d36dd273787s://medium.com/@MRWwebDesign/responsive-images-the-sizes-attribute-and-unexpected-image-sizes-882a2eadb6db


### TODO

- What happens if this plugin is used as a mu-plugin?
- Add a mention of the UI-based settings plugin (once it's ready).


### CHANGE LOG

__-- 0.0.9 - Mon 24 October 2022__

- ADDED: the boilerplate for the new filter: srcsetSources() in the main plugin.
- NO CHANGE: Plugin version #.

__-- 0.0.8 - Mon 5 July 2021__

- Massive refactoring of this plugin, as well as the main plugin.


__-- 0.0.5 - Tue 15 Oct 2019__

- INIT - Hey! Ho!! Let's go!!!
